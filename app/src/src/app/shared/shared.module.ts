import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DataService, LocationApiService } from './services';
import { AuthenticationService } from 'src/app/shared/services/authentication.service';
import { JwtInterceptor, ErrorInterceptor } from './helpers';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { TransportationDirective } from './directives/transportation.directive';
import { AttendeesService } from './services/attendees.service';
import { StoreModule, MetaReducer } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { reducers, effects } from './store';

@NgModule({
  declarations: [
    TransportationDirective,
  ],
  imports: [
    CommonModule,
    StoreModule.forFeature('app', reducers),
    EffectsModule.forFeature(effects),
  ],
  providers: [
    AuthenticationService,
    AttendeesService,
    DataService,
    LocationApiService,
    {provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    {provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true }
  ],
})
export class SharedModule { }
