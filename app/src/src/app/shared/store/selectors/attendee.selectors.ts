import {createSelector} from '@ngrx/store';

import * as fromFeature from '../reducers';
import * as fromAttendee from '../reducers/attendee.reducer';

export const getAttendeeState = createSelector(
    fromFeature.getAppState,
    (state: fromFeature.AppState) => state.donations
);

export const getAttendeeDonationEntities = createSelector(
    getAttendeeState,
    fromAttendee.getDonationEntities
);


export const getAllAttendeeDonations = createSelector(getAttendeeDonationEntities, entities => {
    if (!entities) {
        return [];
    }
    return Object.keys(entities).map(id => entities[id]);
});

export const getAttendeeDonationsLoaded = createSelector(
  getAttendeeState,
  fromAttendee.getLoaded
);
export const getAttendeeDonationsLoading = createSelector(
  getAttendeeState,
  fromAttendee.getLoading
);
