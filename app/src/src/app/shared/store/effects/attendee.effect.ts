import { Injectable } from '@angular/core';

import { Action } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable, of, zip, merge } from 'rxjs';
import { catchError, map, mergeMap, combineAll, mergeAll } from 'rxjs/operators';
import * as attendeesAction from '../actions/attendee.action';
import { AttendeesService } from 'src/app/shared/services/attendees.service';
import { EditAttendeePasswordModel } from 'src/app/modules/account/models/edit_attendee_password.model';
import { EditAttendeeProfileModel } from 'src/app/modules/account/models/edit_attendee.model';
import { EditAttendeeTourProfileModel } from 'src/app/modules/account/models/edit_attendee_profile.model';
import { TourAttendeeDonation } from 'src/app/shared/models/tour_attendee_donation.model';
import { DonationsService } from '../../services/donations.service';
// https://management.tourdeplanet.org/auth/login
@Injectable()
export class AttendeeEffect {
    constructor(
        private actions$: Actions,
        private service: AttendeesService,
        private donationService: DonationsService
    ) {}
    @Effect()
    updateAttendeePassword$: Observable<Action> = this.actions$.pipe(
        ofType(attendeesAction.UPDATE_ATTENDEE_PASSWORD),
        map((action: attendeesAction.UpdateAttendeePasswordAction) => action.payload),
        mergeMap((password: EditAttendeePasswordModel) => this.service.updateTourAttendeePassword(password)),
        map(mappedTour => new attendeesAction.UpdateAttendeePasswordSuccessAction(mappedTour)),
        catchError(error => of(new attendeesAction.UpdateAttendeePasswordFailAction(error)))
    );

    @Effect()
    updateAttendeeProfile$: Observable<Action> = this.actions$.pipe(
        ofType(attendeesAction.UPDATE_ATTENDEE_PROFILE),
        map((action: attendeesAction.UpdateAttendeeProfile) => action.payload),
        mergeMap((profile: EditAttendeeProfileModel) => this.service.updateTourAttendeeProfile(profile)),
        map(profile => new attendeesAction.UpdateAttendeeProfile(profile)),
        catchError(error => of(new attendeesAction.UpdateAttendeeProfile(error)))
    );

    @Effect()
    updateAttendeeTourProfile$: Observable<Action> = this.actions$.pipe(
        ofType(attendeesAction.UPDATE_ATTENDEE_TOUR_PROFILE),
        map((action: attendeesAction.UpdateAttendeeTourProfileAction) => action.payload),
        mergeMap((profile: EditAttendeeTourProfileModel) => this.service.updateTourProfile(profile)),
        map(profile => new attendeesAction.UpdateAttendeeTourProfileAction(profile)),
        catchError(error => of(new attendeesAction.UpdateAttendeeTourProfileAction(error)))
    );
    @Effect()
    loadAttendeeDonations$: Observable<Action> = this.actions$.pipe(
        ofType(attendeesAction.LOAD_ATTENDEE_DONATION),
        mergeMap(() => this.donationService.getAttendeeDonations()),
        map((donations: TourAttendeeDonation[]) => new attendeesAction.LoadAttendeeDonationSuccesscAction(donations)),
        catchError(error => of(new attendeesAction.LoadAttendeeDonationFailAction(error)))
    );

    @Effect()
    addAttendeeDonation$: Observable<Action> = this.actions$.pipe(
        ofType(attendeesAction.ADD_ATTENDEE_DONATION),
        map((action: attendeesAction.AddAttendeeDonation) => action.payload),
        mergeMap((donation: TourAttendeeDonation) => this.donationService.addAttendeeDonation(donation)),
        map(donation => new attendeesAction.AddAttendeeDonationSuccess(donation)),
        catchError(error => of(new attendeesAction.AddAttendeeDonationFail(error)))
    );
}
