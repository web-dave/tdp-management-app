import {createSelector} from '@ngrx/store';

import * as fromFeature from '../../default/store/reducers';
import * as fromMOT from '../reducers/mot_data.reducer';

export const getMeansOfTransportationState = createSelector(
    fromFeature.getDataState,
    (state: fromFeature.DataState) => state.mot
);

export const getMeansOfTransportationEntities = createSelector(
    getMeansOfTransportationState,
    fromMOT.getMeansOfTransportationEntities
);

export const getMeansOfTransportation = createSelector(getMeansOfTransportationEntities, entities => {
    return Object.keys(entities).map(id => entities[id]);
});
