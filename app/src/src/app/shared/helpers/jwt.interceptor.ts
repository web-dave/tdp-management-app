import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { AuthenticationService } from 'src/app/shared/services/authentication.service';


@Injectable()
export class JwtInterceptor implements HttpInterceptor {
    constructor(private authenticationService: AuthenticationService) { }
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // add authorization header with jwt token if available
        const currentUserToken = this.authenticationService.currentUserTokenValue;
        if (currentUserToken && currentUserToken.accessToken) {
            request = request.clone({
                setHeaders: {
                    Authorization: `Bearer ${currentUserToken.accessToken}`
                }
            });
        }

        return next.handle(request);
    }
}
