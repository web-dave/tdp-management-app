import { Injectable } from '@angular/core';
import { TourAttendeeDonation } from '../models/tour_attendee_donation.model';
import { MeanOfTransportationModel } from 'src/app/shared/models/mean_of_transportaion.model';

const DEFAULT_COLOR_CLASS = 'tdp-hiking';

@Injectable({
  providedIn: 'root'
})
export class DonationHelperService {

  constructor() { }

  public static backgroundColorForMost(donations: TourAttendeeDonation[]): MeanOfTransportationModel {
    const amountsByMoTId: {[id: string]: number} = {};
    const mots = {};
    donations.forEach(d => {
      if (d.hasOwnProperty('by') ) {
        if (!d.by) {
          return;
        }
        const by = d.by as string;
        if (!amountsByMoTId.hasOwnProperty(by)) {
          amountsByMoTId[by] = 0;
        }
        amountsByMoTId[by] += d.amount;
        mots[by] = d.by;
      }
    });
    const sortedMots = Object.keys(amountsByMoTId).sort((a, b) => {
      return amountsByMoTId[a] - amountsByMoTId[b];
    });

    let counter = 0;
    sortedMots.forEach((value: string, index: number) => {
      counter++;
      if (1 === counter) {
        return mots[index];
      }
    });

    return null;
  }

  public static sumDonation(donations: TourAttendeeDonation[]): number {
    return donations.reduce((acc, d) => {
      return acc + d.amount;
    }, 0);
  }

  public static getColor(motId: string, mots: {[id: string]: MeanOfTransportationModel}): string {
    return mots.hasOwnProperty(motId) ? mots[motId].color : '';
  }

  public static getIconClass(motId: string, mots: {[id: string]: MeanOfTransportationModel}): string {
    return mots.hasOwnProperty(motId) ? mots[motId].iconClass : '';
  }
}
