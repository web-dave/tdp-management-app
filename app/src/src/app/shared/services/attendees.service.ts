import {Injectable} from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { catchError } from 'rxjs/operators';
import 'rxjs/add/observable/throw';

import { environment } from '../../../environments/environment';
import { EditAttendeeTourProfileModel } from 'src/app/modules/account/models/edit_attendee_profile.model';
import { EditAttendeePasswordModel } from 'src/app/modules/account/models/edit_attendee_password.model';
import { EditAttendeeProfileModel } from 'src/app/modules/account/models/edit_attendee.model';
import { TourAttendeeRegistrationModel } from 'src/app/modules/account/models/tour_attendee_registration.model';
import { AuthenticationService } from 'src/app/shared/services/authentication.service';

@Injectable()
export class AttendeesService {
    constructor(private http: HttpClient, private auth: AuthenticationService) {}

    create(registration: TourAttendeeRegistrationModel): Observable<{} | string> {
      return this.http
            .post<TourAttendeeRegistrationModel>(`${environment.backendUrl}/register`, {
                username: registration.username,
                email: registration.mail,
                plainPassword: registration.password,
                newsletterAccepted: false,
                termsAccepted: true
            })
            .pipe(catchError((error: any) => {
                return Observable.throw(error);
            }));
    }

    getCurrentTourAttendeeProfile(userId?: string): Observable<EditAttendeeTourProfileModel> {
      userId = userId || this.getCurrentUserId();
      return this.http
          .get<EditAttendeeTourProfileModel>(`${environment.backendUrl}/users/${userId}/attendee-tour-profile`)
          .pipe(catchError((error: any) => {
              return Observable.throw(error);
          }));
    }

    updateTourProfile(attendee: EditAttendeeTourProfileModel, userId?: string): Observable<EditAttendeeTourProfileModel> {
      userId = userId || this.getCurrentUserId();
      return this.http
          .patch<EditAttendeeTourProfileModel>(`${environment.backendUrl}/users/${userId}/attendee-tour-profile`, attendee)
          .pipe(catchError((error: any) => {
              return Observable.throw(error);
          }));
    }

    getCurrentAttendeeProfile(userId?: string): Observable<EditAttendeeProfileModel> {
      userId = userId || this.getCurrentUserId();
      return this.http
          .get<EditAttendeeProfileModel>(`${environment.backendUrl}/users/${userId}/attendee-profile`)
          .pipe(catchError((error: any) => {
              return Observable.throw(error);
          }));
    }

    updateTourAttendeePassword(attendee: EditAttendeePasswordModel, userId?: string): Observable<EditAttendeePasswordModel> {
      userId = userId || this.getCurrentUserId();
      return this.http
          .patch<EditAttendeePasswordModel>(`${environment.backendUrl}/users/${userId}/attendee-password`, attendee)
          .pipe(catchError((error: any) => {
              return Observable.throw(error);
          }));
    }

    updateTourAttendeeProfile(attendee: EditAttendeeProfileModel, userId?: string): Observable<EditAttendeeProfileModel> {
      userId = userId || this.getCurrentUserId();
      return this.http
          .patch<EditAttendeeProfileModel>(`${environment.backendUrl}/users/${userId}/attendee-profile`, attendee)
          .pipe(catchError((error: any) => {
              return Observable.throw(error);
          }));
    }

    private getCurrentUserId(): string {
      const userToken = this.auth.currentUserTokenValue;
      if (null == userToken) {
        return '';
      }

      return userToken.userValues.userId;
    }
}
