import { FormControl } from '@angular/forms';

export enum TourOrganizerType {
    SINGLE = 'single',
    GROUP = 'group',
}
export interface TourOrganizerModel {
    id?: number;
    name: string;
    type: TourOrganizerType.SINGLE | TourOrganizerType.GROUP;
}

export class TourOrganizerFormModel {
    id = new FormControl();
    name = new FormControl();
    type = new FormControl();

    constructor(organizer: TourOrganizerModel) {
        if (organizer.id) {
            this.id.setValue(organizer.id);
        }
        this.name.setValue(organizer.name);
        this.type.setValue(organizer.type);
    }
}
