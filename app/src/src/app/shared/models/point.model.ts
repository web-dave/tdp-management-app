import { FormControl } from '@angular/forms';

export interface Point {
    latitude: number;
    longitude: number;
    name?: string;
}

export class PointForm {
    latitude = new FormControl();
    longitude = new FormControl();
    name = new FormControl();

    constructor(point: Point) {
        this.latitude.patchValue(point.latitude);
        this.longitude.patchValue(point.longitude);
        this.name.patchValue(point.name);
    }
}
