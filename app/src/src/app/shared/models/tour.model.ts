import { TourSegmentModel, TourSegmentFormModel } from './tour_segment.model';
import { TourOrganizerModel, TourOrganizerFormModel } from './tour_organizer.model';
import { TourPoint, TourPointForm } from './tour_point.model';
import { FormControl, FormArray, FormBuilder, FormGroup } from '@angular/forms';

export enum TourType {
    BIKE = 'directions_byke',
    WALK = 'directions_run',
    RIDE_HORSE = 'directions',
    SCOOTER = 'ev_station',
}

export enum TourState {
    DRAFT = 'draft',
    INTERNAL = 'internal',
    PUBLISHED = 'published',
    VERIFIED = 'verified',
}

export interface TourModel {
    id: number;
    title: string;
    description: string;
    organizer: TourOrganizerModel;
    tourSegments: TourSegmentModel[];
    start: TourPoint;
    end: TourPoint;
    type: TourType.BIKE | TourType.WALK | TourType.RIDE_HORSE | TourType.SCOOTER;
    state: TourState.DRAFT | TourState.INTERNAL | TourState.PUBLISHED | TourState.VERIFIED;
}

export class TourForm {
    id = new FormControl();
    title = new FormControl();
    organizer = new FormControl();
    description = new FormControl();
    tourSegments = new FormArray([]);
    start = new FormControl();
    end = new FormControl();
    type = new FormControl();
    state = new FormControl();

    constructor(tour: TourModel, fb: FormBuilder) {
        if (tour.id) {
            this.id.setValue(tour.id);
        }
        this.state.setValue(tour.state);
        this.type.setValue(tour.type);
        this.title.setValue(tour.title);
        this.organizer.setValue(fb.group(new TourOrganizerFormModel(tour.organizer)));
        tour.tourSegments.forEach(segment => {
            this.tourSegments.push(fb.group(new TourSegmentFormModel(segment, fb)));
        });

        this.start.setValue(fb.group(new TourPointForm(tour.start)));
        this.end.setValue(fb.group(new TourPointForm(tour.end)));
        this.description.setValue(tour.description);
    }
}
