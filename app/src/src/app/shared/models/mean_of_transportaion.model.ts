export class MeanOfTransportationModel {
    id: string;
    name: string;
    iconClass: string;
    color: string;
}
