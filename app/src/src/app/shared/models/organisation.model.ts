export class Organisation {
    id: number;
    name: string;
    shortName: string;
}
