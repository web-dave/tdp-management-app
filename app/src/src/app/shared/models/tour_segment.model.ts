import { TourPoint, TourPointForm } from './tour_point.model';
import { TourModel } from './tour.model';
import { FormControl, FormBuilder } from '@angular/forms';

export class TourSegmentModel {
    id: number;
    name?: string;
    description: string;
    start: TourPoint;
    end: TourPoint;
    tour?: TourModel;
}

export class TourSegmentFormModel {
    id = new FormControl();
    name = new FormControl();
    description = new FormControl();
    start = new FormControl();
    end = new FormControl();
    date = new FormControl();

    constructor(segment: TourSegmentModel, fb: FormBuilder) {
        if (segment.id) {
            this.id.setValue(segment.id);
        }
        this.name.setValue(segment.name);
        this.description.setValue(segment.description);
        this.start.setValue(fb.group(new TourPointForm(segment.start)));
        this.end.setValue(fb.group(new TourPointForm(segment.end)));
    }
}
