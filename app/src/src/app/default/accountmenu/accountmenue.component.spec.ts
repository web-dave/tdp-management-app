import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountMenueComponent } from './accountmenue.component';

describe('MenueComponent', () => {
  let component: AccountMenueComponent;
  let fixture: ComponentFixture<AccountMenueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountMenueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountMenueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
