import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/shared/services/authentication.service';
import { Store } from '@ngrx/store';
import * as fromAttendeeStore from 'src/app/shared/store';
import { NotificationService } from 'src/app/modules/notification-module/notification.service';
import { User } from 'src/app/modules/account/models/user.model';
import { Observable } from 'rxjs';
import { UserToken } from 'src/app/shared/models/user.model';

@Component({
  selector: 'app-default',
  templateUrl: './default.component.html'
})
export class DefaultComponent implements OnInit {
  user: User;
  title = 'TdP - Management';
  menuVisible = false;
  accountMenuVisible = false;
  userToken$: Observable<UserToken>;
  public constructor(
      private authService: AuthenticationService,
      private store: Store<fromAttendeeStore.AppState>,
      private notify: NotificationService
  ) {}

  ngOnInit(): void {
    this.userToken$ = this.authService.currentUserToken;
    this.userToken$.subscribe(userToken => {
      this.user = null !== userToken ? userToken.userValues : null;
      if (null !== userToken) {
        this.store.dispatch(new fromAttendeeStore.LoadAttendeeDonationAction());
      } else {
        this.notify.info('Please login or register to edit your own climate friendly movements');
      }
    });
  }

  toggleMenu(): void {
    this.menuVisible = !this.menuVisible;
    if (this.menuVisible ) {
      setTimeout(() => {
        this.menuVisible = false;
      }, 5000);
    }
  }

  toggleAccountMenu(): void {
    this.accountMenuVisible = !this.accountMenuVisible;
    if (this.accountMenuVisible ) {
      setTimeout(() => {
        this.accountMenuVisible = false;
      }, 5000);
    }
  }
}
