import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { Router } from '@angular/router';
import { UserToken } from '../../shared/models/user.model';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  @Input() user: UserToken;
  @Output() toggleMenu: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() toggleAccountMenu: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(private router: Router) { }

  ngOnInit(): void {
  }
  get isHome(): boolean {
    return this.router.url === '/';
  }

  get isUser(): boolean {
    return !!(this.user);
  }

  onToggleMenu(): void {
    this.toggleMenu.emit(true);
  }
  onToggleAccountMenu(): void {
    this.toggleAccountMenu.emit(true);
  }
}
