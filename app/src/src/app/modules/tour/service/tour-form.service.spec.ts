import { TestBed } from '@angular/core/testing';

import { TourFormService } from './tour-form.service';

describe('TourFormService', () => {
  let service: TourFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TourFormService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
