import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TourComponent } from './components/tour/tour.component';
import { Routes } from '@angular/router';
import { RouterModule } from '@angular/router';
import {
  MatCardModule,
  MatListModule,
  MatExpansionModule,
  MatFormFieldModule,
  MatIconModule,
  MatButtonModule,
  MatSelectModule,
  MatDatepickerModule,
  MatAutocompleteModule,
  MatNativeDateModule,
  MatInputModule} from '@angular/material';
import { SharedModule } from 'src/app/shared/shared.module';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { TourFormComponent } from './components/tour-form/tour-form.component';
import { OrganizerFormComponent } from './components/organizer-form/organizer-form.component';
import { TourPointFormComponent } from './components/tour-point-form/tour-point-form.component';
import { TourSegmentFormComponent } from './components/tour-segment-form/tour-segment-form.component';
import { TourEditComponent } from './components/tour-edit.component/tour-edit.component.component';
import { AddTourComponent } from './components/add-tour.component/add-tour.component.component';
import { TourListRowComponent } from './components/tour-list-row/tour-list-row.component';

export const TOUR_ROUTES: Routes = [
  { path: 'dashboard', component: TourComponent },
  { path: 'edit-tour/[:id]', component: TourEditComponent },
  { path: 'add-tour', component: AddTourComponent },
];


@NgModule({
  declarations: [
    TourComponent,
    TourFormComponent,
    OrganizerFormComponent,
    TourPointFormComponent,
    TourSegmentFormComponent,
    TourEditComponent,
    AddTourComponent,
    TourFormComponent,
    TourListRowComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(TOUR_ROUTES),
    SharedModule,
    MatAutocompleteModule,
    MatButtonModule,
    MatCardModule,
    MatDatepickerModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatNativeDateModule,
    MatSelectModule,
    ReactiveFormsModule,
    HttpModule,
    HttpClientModule,
  ],
  exports: [TourComponent]
})
export class TourModule { }
