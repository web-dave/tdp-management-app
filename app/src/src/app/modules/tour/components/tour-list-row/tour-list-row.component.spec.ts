import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TourListRowComponent } from './tour-list-row.component';

describe('TourListRowComponent', () => {
  let component: TourListRowComponent;
  let fixture: ComponentFixture<TourListRowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TourListRowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TourListRowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
