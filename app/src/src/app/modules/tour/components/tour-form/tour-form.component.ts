import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormArray} from '@angular/forms';
import { Subscription } from 'rxjs';
import { TourFormService } from '../../service/tour-form.service';
import { TourModel, TourType, TourState } from '../../../../shared/models/tour.model';
import * as fromTourStore from 'src/app/shared/store';
import * as fromMapStore from 'src/app/shared/store';
import { Store } from '@ngrx/store';
import { TourPoint } from 'src/app/shared/models/tour_point.model';

@Component({
  selector: 'app-tour-form',
  templateUrl: './tour-form.component.html',
  styleUrls: ['./tour-form.component.scss']
})
export class TourFormComponent implements OnInit {
  @Input() tour: TourModel;
  tourForm: FormGroup;
  tourFormSub: Subscription;
  formInvalid = false;
  tourSegments: FormArray;
  start: FormGroup;
  end: FormGroup;
  organizer: FormGroup;
  types = [TourType.BIKE, TourType.WALK, TourType.SCOOTER, TourType.RIDE_HORSE];
  states = [TourState.DRAFT, TourState.INTERNAL, TourState.PUBLISHED, TourState.VERIFIED];

  constructor(private tourFormService: TourFormService, private store: Store<fromTourStore.AppState>) { }

  ngOnInit(): void {
    if (this.tour) {
      this.tourFormService.init(this.tour);
      this.tourFormSub = this.tourFormService.tourForm$.subscribe(form => {
        this.tourForm = form;
        this.tourSegments = this.tourForm.get('tourSegments') as FormArray;
        this.organizer = this.tourForm.get('organizer') as FormGroup;
        this.start = this.tourForm.get('start') as FormGroup;
        this.end = this.tourForm.get('end') as FormGroup;
      });
    }
  }

  addSegment() {
    this.tourFormService.addSegment();
  }

  onDeleteSegment(index: number): void {
    this.tourFormService.deleteSegment(index);
  }

  onUpdateTour(): void {
    this.store.dispatch(new fromTourStore.UpdateTour(this.tourFormService.getModelData()));
  }

  onCreateTour(): void {
    this.store.dispatch(new fromTourStore.CreateTour(this.tourFormService.getModelData()));
  }

  onDeleteTour(): void {
    this.store.dispatch(new fromTourStore.RemoveTour(this.tour));
  }

  onAddPoint(point: TourPoint): void {
    this.store.dispatch(new fromMapStore.UpdateTourInWorkingMode(this.tour));
  }
}
