import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecentlyAddedDonationsComponent } from './recently-added-donations.component';

describe('RecentlyAddedDonationsComponent', () => {
  let component: RecentlyAddedDonationsComponent;
  let fixture: ComponentFixture<RecentlyAddedDonationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecentlyAddedDonationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecentlyAddedDonationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
