import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DonationRowComponent } from './donation-row.component';

describe('DonationRowComponent', () => {
  let component: DonationRowComponent;
  let fixture: ComponentFixture<DonationRowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DonationRowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DonationRowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
