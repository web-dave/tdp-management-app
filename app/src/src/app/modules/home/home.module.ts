import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CounterComponent } from './components/counter/counter.component';
import { DonationRowComponent } from './components/donation-row/donation-row.component';
import { HomeComponent } from './components/home/home.component';
import { RecentlyAddedDonationsComponent } from './components/recently-added-donations/recently-added-donations.component';
import { TotalDonationsComponent } from './components/total-donations/total-donations.component';
import { RouterModule } from '@angular/router';



@NgModule({
  declarations: [
    CounterComponent,
    DonationRowComponent,
    HomeComponent,
    RecentlyAddedDonationsComponent,
    TotalDonationsComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
  ]
})
export class HomeModule { }
