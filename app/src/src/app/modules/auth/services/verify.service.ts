import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { map, catchError, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class VerifyService {

  constructor(private http: HttpClient) { }

  sendToken(token: string): Observable<any> {
    return this
      .http
      .post<{token: string}>(`${environment.backendUrl}/verify`, {token});
  }
}
