import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { RegistrationFormComponent } from './components/registration/registration/registration.component';
import { VerificationComponent } from './components/verification/verification.component';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';
import { NotificationModule } from '../notification-module/notification.module';
import { LogoutComponent } from './components/logout/logout.component';
import { AuthGuard } from 'src/app/shared/helpers';

const AUTH_ROUTES: Routes = [
  {
    path: 'logout',
    component: LogoutComponent,
    canActivate: [AuthGuard],
  },
  { path: 'verify/:verificationToken', component: VerificationComponent},
  { path: 'login', component: LoginComponent},
  { path: 'register', component: RegistrationFormComponent},
];

@NgModule({
  declarations: [LoginComponent, VerificationComponent, LoginComponent, RegistrationFormComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(AUTH_ROUTES),
    SharedModule,
    ReactiveFormsModule,
    NotificationModule
  ]
})
export class AuthModule { }
