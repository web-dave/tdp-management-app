import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { NotificationService } from '../notification.service';
import { NotificationType } from '../notification_message.model';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss']
})
export class NotificationComponent implements OnInit, OnDestroy {
  private subscription: Subscription;
  message: any = {title: null, text: null};

  constructor(private notificationService: NotificationService) { }

  ngOnInit() {
      this.subscription = this.notificationService.getMessage().subscribe(message => {
          this.message = message;
      });
  }

  onClose(): void {
    this.notificationService.close();
  }

  ngOnDestroy() {
      this.subscription.unsubscribe();
  }

}
