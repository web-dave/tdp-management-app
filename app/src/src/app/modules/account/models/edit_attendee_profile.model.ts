import { Point } from '../../../shared/models/point.model';
import { FormControl } from '@angular/forms';
import { MeanOfTransportationModel } from './../../../shared/models/mean_of_transportaion.model';

export interface EditAttendeeTourProfileModel {
    isBuildOwnTour: boolean;
    isJoinOtherTour: boolean;
    isPress: boolean;
    isOrga: boolean;
    home: Point;
    daysToParticipate: number;
    distanceToParticipate: number;
    meanOfTransportation: string|MeanOfTransportationModel;
    unit: string;
}


export class EditAttendeeTourProfileForm {
    id = new FormControl();
    isBuildOwnTour = new FormControl();
    isJoinOtherTour = new FormControl();
    isPress = new FormControl();
    isOrga = new FormControl();
    home = new FormControl();
    daysToParticipate = new FormControl();
    distanceToParticipate = new FormControl();
    meanOfTransportation = new FormControl();
    unit = new FormControl();
    constructor(model: EditAttendeeTourProfileModel) {
        this.isBuildOwnTour.patchValue(model.isBuildOwnTour);
        this.isOrga.patchValue(model.isOrga);
        this.isPress.patchValue(model.isPress);
        this.isJoinOtherTour.patchValue(model.isJoinOtherTour);
        this.daysToParticipate.patchValue(model.daysToParticipate);
        this.distanceToParticipate.patchValue(model.distanceToParticipate);
        this.unit.patchValue(model.unit);
        this.home.patchValue(model.home);
        this.meanOfTransportation.patchValue(model.meanOfTransportation);
    }
}
