import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AttendeeDonationComponent } from './attendee-donation.component';

describe('AttendeeDonationComponent', () => {
  let component: AttendeeDonationComponent;
  let fixture: ComponentFixture<AttendeeDonationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AttendeeDonationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AttendeeDonationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
