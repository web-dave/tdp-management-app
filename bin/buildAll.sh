#!/usr/bin/env sh

set -ex

VERSION_TAG=${VERSION_TAG-'0.0.1'}
DOCKER_IMAGE=${DOCKER_IMAGE-'registry.gitlab.com/developersforfuture/tdp-management-app'}


# docker pull $DOCKER_IMAGE/node-builder:latest || true
# docker build ./ --target node-builder -t $DOCKER_IMAGE/node-builder:latest \
#     --cache-from $DOCKER_IMAGE/node-builder:latest
# docker push $DOCKER_IMAGE/node-builder:latest

# docker pull $DOCKER_IMAGE/node-dev-base:latest || true
# docker build ./ --target node-dev-base -t $DOCKER_IMAGE/node-dev-base:latest \
#     --cache-from $DOCKER_IMAGE/node-builder:latest \
#     --cache-from $DOCKER_IMAGE/node-dev-base:latest
# docker push $DOCKER_IMAGE/node-dev-base:latest

docker pull $DOCKER_IMAGE/production:$VERSION_TAG || true
docker build ./ --target tdp-app-production -t $DOCKER_IMAGE/production:$VERSION_TAG \
    --cache-from $DOCKER_IMAGE/node-builder:latest \
    --cache-from $DOCKER_IMAGE/production:$VERSION_TAG
docker push $DOCKER_IMAGE/production:$VERSION_TAG

# docker pull $DOCKER_IMAGE/development:$VERSION_TAG || true
# docker build  ./ --target tdp-app-development -t $DOCKER_IMAGE/development:$VERSION_TAG \
#     --cache-from $DOCKER_IMAGE/development:$VERSION_TAG \
#     --cache-from $DOCKER_IMAGE/node-builder:latest \
#     --cache-from $DOCKER_IMAGE/node-dev-base:latest
# docker push $DOCKER_IMAGE/development:$VERSION_TAG
